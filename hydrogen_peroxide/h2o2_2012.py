from typing import Callable
import numpy as np
from opt_einsum import contract
import os
import dill
from polylib.PES._base import BasePES
from polylib import units, elements, numderiv as nd
import symnummath as snm


def dihedral(x1, x2, x3, x4, sympy_mode=False):
	"""
	formulas from wikipedia, returns an angle from -pi to pi
	function taken from polylib
	https://en.wikipedia.org/wiki/Dihedral_angle
	"""
	xA = x2 - x1
	xB = x3 - x2
	xC = x4 - x3
	AcrossB = np.cross(xA,xB)
	BcrossC = np.cross(xB,xC)
	Bnorm = xB.dot(xB)**(1./2)
	a = Bnorm*np.dot(xA,BcrossC)
	b = np.dot(AcrossB,BcrossC)
	res = snm.arctan2(a, b, sympy_mode=sympy_mode)
	return res

def dihedral2(x1, x2, x3, x4, sympy_mode=False):
	"""
	An alternative definition using arccos
	https://en.wikipedia.org/wiki/Dihedral_angle
	Definition range: 0 to pi
	"""
	xA = x2 - x1
	xB = x3 - x2
	xC = x4 - x3
	AcrossB = np.cross(xA,xB)
	BcrossC = np.cross(xB,xC)
	ABcrossnorm = np.dot(AcrossB, AcrossB)**(1./2.)
	BCcrossnorm = np.dot(BcrossC, BcrossC)**(1./2.)
	res = snm.arccos(np.dot(AcrossB, BcrossC)/(ABcrossnorm * BCcrossnorm),  sympy_mode=sympy_mode)
	return res


class h2o2_12(BasePES):

	def __init__(self, atomlist: list[str] = ['H','O','O','H'], bo_corrected: bool = True, sympy_mode: bool = False):
		"""
		PES source: https://onlinelibrary.wiley.com/doi/full/10.1002/jcc.23137
		bo_corrected -> do we want the adiabatic version, or the version with Born-Oppenheimer corrections? (you probably want this to be always True)
		sympy_mode -> If functions have a sympy_mode parameter, it influences what type they take
		"""
		super().__init__()
		self.UNITS = units.hartAng() #TODO: think about this still
		self.atomlist = atomlist
		self.mass = elements.getmass(self.atomlist) * self.UNITS.amu
		self.curr_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
		self.bo_corrected = bo_corrected
		if self.bo_corrected:
			name = self.curr_dir + "fit_coeffs/coeffs_bo.txt"
			self.OO_0  = 1.45539378
			self.HO_0  = 0.96252476
			self.OOH_0 = np.radians(101.08194717)
		else:
			name = self.curr_dir + "fit_coeffs/coeffs_adiabatic.txt"
			self.OO_0  = 1.45538654
			self.HO_0  = 0.96257063
			self.OOH_0 = np.radians(101.08307909)
		whole = np.loadtxt(name)
		self.c_index = np.asarray(whole[:,:6], dtype=int)
		self.c_val = whole[:, -1:]
		self.sympy_mode = sympy_mode
		if sympy_mode:
			import symderiv as sd # Custom library for symbolical derivatives
			self.sd = sd

	def potential(self, x: np.ndarray):
		x = x.flatten()
		q = self.converter(x)
		res = self.V(q)
		return res

	def potential_der(self, x, dern):
		x = x.flatten()
		q = self.converter(x, sympy_mode=False)

		#Loading all needed derivatives
		C = [0]
		V = [0]
		C1 = np.array(self.converter_der(x, 1))
		V1 = np.array(self.Vder(q, 1))
		C.append(C1)
		V.append(V1)
		if dern > 1:
			C2 = np.array(self.converter_der(x, 2))
			V2 = np.array(self.Vder(q, 2))
			C.append(C2)
			V.append(V2)
		if dern > 2:
			C3 = np.array(self.converter_der(x, 3))
			V3 = np.array(self.Vder(q, 3))
			C.append(C3)
			V.append(V3)
		if dern > 3:
			C4 = np.array(self.converter_der(x, 4))
			V4 = np.array(self.Vder(q, 4))
			C.append(C4)
			V.append(V4)

		res = snm.chain_rule(V, C, dern)
		return res

	def gradient(self, x: np.ndarray):
		res = self.potential_der(x, 1)
		return res.reshape(4, 3)

	def hessian(self, x: np.ndarray):
		res = self.potential_der(x, 2)
		return res

	def third_der(self, x: np.ndarray):
		res = self.potential_der(x, 3)
		return res

	def fourth_der(self, x: np.ndarray):
		res = nd.fourth4fromhess(x, self.hessian, h=1.e-3)
		#res = self.potential_der(x, 4)
		return res

	def numthird(self, x: np.ndarray, h=1.e-3):
		res = nd.third4fromhess(x, self.hessian, h)
		return res

	def V(self, q: np.ndarray):
		res = 0
		tot = len(self.c_val[:,0])
		for i in range(tot):
			res += self._summand(q, i)
		return res

	def Vder(self, q: np.ndarray, dern: int):
		res = np.zeros((6,) * dern)
		tot = len(self.c_val[:,0])
		for i in range(tot):
			res += np.array(self._summand_der(q, i, dern))
		return res

	def _summand(self, q: np.ndarray, n: int):
		"""
		Works for both sympy and numpy.
		"""
		index = self.c_index[n]
		val = self.c_val[n, 0]
		res = 0
		term = 1
		for i in range(5):
			term *= q[i]**index[i]
		term *= val * snm.cos(index[-1]*q[-1], self.sympy_mode)
		res += term
		return res

	def converter(self, x: np.ndarray, sympy_mode=None):
		"""
		Converts cartesian coordinates into internal coordinates
		sympy_mode == True -> accepts sympy symbols
		otherwise numpy
		"""
		if sympy_mode==None:
			sympy_mode = self.sympy_mode
			res = np.zeros((6,), dtype=object)
		else:
			res = np.zeros((6,))

		acos = lambda x: snm.arccos(x, sympy_mode=sympy_mode)
		asin = lambda x: snm.arcsin(x, sympy_mode=sympy_mode)
		#sin  = lambda x: snm.sin(x, sympy_mode=sympy_mode)
		def r(x):
			"""vector norm in numpy and sympy"""
			res = x.dot(x)**(1./2)
			return res

		# Converting to vectors
		N = 3
		x0 = x[0:N]
		x1 = x[N:2*N]
		x2 = x[2*N:3*N]
		x3 = x[3*N:4*N]
		HO = x1 - x0
		OH = x3 - x2
		OO = x2 - x1
		#q1, q2, OH stretching modes
		q1_e = self.HO_0
		q2_e = self.HO_0
		q3_e = self.OO_0
		res[0] = 1. -q1_e/r(HO)
		res[1] = 1. -q2_e/r(OH)
		#q3, OO stretching mode
		res[2] = 1. -q3_e/r(OO)
		#OOH bending coordinates
		OOH_0 = self.OOH_0
		ri = HO
		rj = OO
		rk = OH
		phi_ij = acos(-np.dot(ri, rj)/ (r(ri)*r(rj)))
		phi_jk = acos(-np.dot(rj, rk)/ (r(rj)*r(rk)))

		res[3] = (phi_ij - OOH_0) #* 180./np.pi
		res[4] = (phi_jk - OOH_0) #* 180./np.pi
		#HOOH torsion mode
		#https://www.tandfonline.com/doi/abs/10.1080/00268977200102361
		#eq. 16a
		#cross1 = 0
		#cross2 = 0
		res[5] = dihedral(x0, x1, x2, x3, sympy_mode=sympy_mode)
		#res[5] = asin(
		#	HO.dot(np.cross(OO, OH)) / (r(HO) * r(OO) * r(OH) \
		#	* sin(phi_ij) * sin(phi_jk) )
		#)
		return res


	def print_coords(self, x: np.ndarray, no: int = -1) -> None:
		"""
		Prints internal coordinates. The input has to be cartesian.
		"""
		q = self.converter(x.flatten(), sympy_mode=False)
		print("####################################")
		print("Internal coordinate overview")
		if no > -1:
			if no == 1 or no == 0:
				print("OH stretching mode (A):")
				print(q[0])
				print(q[1])
			elif no == 2:
				print("OO stretching mode (A):")
				print(q[2])
			elif no == 3 or no == 4:
				print("OOH bending angle (dg):")
				print((q[3] + self.OOH_0)* 180./np.pi)
				print((q[4] + self.OOH_0)* 180./np.pi)
			else:
				print("HOOH torsion mode (dg):")
				print(np.degrees(q[5]))
		else:

			print("OH stretching modes (A):")
			print(q[0])
			print(q[1])
			print("OO stretching mode (A):")
			print(q[2])
			print("OOH bending angle (dg):")
			print((q[3] + self.OOH_0)* 180./np.pi)
			print((q[4] + self.OOH_0)* 180./np.pi)
			print("HOOH torsion mode (dg):")
			print(np.degrees(q[5]))
		return

	def _summand_der(self, q: np.ndarray, n: int, dern: int):
		"""Derivative of the summand"""
		oname = self.curr_dir + "derivatives/summand"+str(n)+"der" + str(dern) + ".pkl"
		res = snm.memoized_der(q,lambda q: self._summand(q, n), dern, oname, self.sympy_mode)
		return res

	def converter_der(self, x: np.ndarray, dern: int):
		"""Derivative of the converter"""
		x = x.flatten()
		if True:
			oname = self.curr_dir + "derivatives/convder" + str(dern) + ".pkl"
			res = snm.memoized_der(x, self.converter,  dern, oname, self.sympy_mode)
		else:
			h = 1e-3
			if dern == 1:
				res = nd.grad4(x, self.converter, h)
			elif dern == 2:
				res = nd.hess4(x, self.converter, h)
			elif dern == 3:
				res = nd.thirdtensor4(x, self.converter, h)
			elif dern == 4:
				res = nd.fourthtensor4(x, self.converter, h)
		return res


if __name__ == "__main__":

	PES = h2o2_12()
	PES_symb = h2o2_12(sympy_mode=True)

	x_test = np.array(
		[
			[ 1.039938,    -0.919890,     0.283607],
			[ 0.926931,    -0.000000,    -0.308872],
			[-0.372107,     0.750000,    -0.308874],
			[-0.101543,     1.545759,     0.012939]
		]
	)

	q_test = PES.converter(x_test.flatten())

	#xx = x_test.flatten()
	#print(xx)

	#PES.print_coords(x_test)
	#exit()


	if False:
		print("####################################")
		print("Testing V derivatives")
		from polylib import numderiv as nd
		n_V = 1
		print(n_V)
		Vder = PES.Vder(q_test, n_V)
		print(Vder.shape)

		if n_V==1:
			numVder_f = lambda x, f, h: nd.grad2(x, f, h)
		elif n_V==2:
			numVder_f = nd.hess4
		elif n_V==3:
			numVder_f = nd.thirdtensor2
		elif n_V==4:
			numVder = nd.fourth4fromhess(q_test, lambda q: PES.Vder(q, 2), h=1e-3)

		if n_V < 4:
			numVder = numVder_f(q_test, PES.V, h=1e-3)

		print(f"Shape: {Vder.shape}")
		resV = Vder / np.where(numVder == 0, np.nan, numVder)
		print(f"Max ratio (should be close to 1): {np.nanmax(resV)}")
		print(f"Min ratio (should be close to 1): {np.nanmin(resV)}")
		if n_V <= 2:
			print(resV)
		elif n_V == 3:
			print(resV[0])
		elif n_V == 4:
			print(resV[0, 0])

	if False:
		print("####################################")
		print("Testing converter derivatives")
		from polylib import numderiv as nd
		n_c = 1
		print(n_c)
		Vder = PES_symb.converter_der(x_test, n_c)
		print(Vder.shape)
		exit()
		#
		#convder = PES.Vder(q_test, 3)
		if n_c==1:
			numder_f = lambda x, f, h: nd.grad2(x, f, h, fshape=(6,))
		elif n_c==2:
			numder_f = nd.hess4
		elif n_c==3:
			numder_f = nd.thirdtensor2
		numder = numder_f(x_test.flatten(), PES.converter, h=1e-3)
		if n_c==1:
			numder = numder.reshape((12,6))
		num = np.where(numder == 0, np.nan, numder)
		res = Vder / num
		print(f"Max ratio (should be close to 1): {np.nanmax(res)}")
		print(f"Min ratio (should be close to 1): {np.nanmin(res)}")
		print(res[..., 0])
		print(Vder.shape)

	if True:
		print("####################################")
		print("Stability analysis")
		from polylib import numderiv as nd
		def perturb_rand(x, eps, set_seed=True):
			if set_seed: np.random.seed(999)
			r = (2*np.random.randn(*x.shape) - 1)*eps + 1.
			res = x*r
			return res

		def pos(x):
			res = np.unravel_index(x.argmax(),x.shape)
			return res

		n_c = 2
		print(n_c)
		eps = 1.e-8

		#potder  = PES.potential_der(x_test, n_c)
		#potder2 = PES.potential_der(perturb(x_test, eps), n_c)
		#diff = np.nan_to_num(np.abs(potder2 - potder)/potder)
		#print(np.max(diff))
		#exit()
		def print_difference(eps):
			print("----------------")
			print(f"epsilon: {eps}")
			#convder  = PES_symb.converter_der(x_test, n_c)
			#exit()
			convder  = PES.converter_der(x_test, n_c)
			convder2 = PES.converter_der(perturb_rand(x_test, eps), n_c)
			x_pert = perturb_rand(x_test, eps)
			q_pert = PES.converter(x_pert.flatten())
			#PES.print_coords(x_test.flatten())
			#PES.print_coords(x_pert.flatten())
			#exit()

			diff = np.nan_to_num(np.abs(convder2 - convder)/convder)
			print(convder.shape)
			#print(diff)

			#print(diff[1, 11])
			#print(diff[1, 10])
			print( convder[10, 2])
			print(convder2[10, 2])
			print(    diff[10, 2])
			#exit()
			# diff[diff > 10**10] = 0
			print(f"Convder max_err: {np.max(diff)}")
			print(pos(diff))

			Vder  = PES.Vder(q_test, n_c)
			Vder2 = PES.Vder(perturb_rand(q_test, eps), n_c)
			diff = np.nan_to_num(np.abs(Vder2 - Vder)/Vder)
			print(f"Vder max_err: {np.max(diff)}")

		for i in range(8):
			print_difference(eps*10**i)
			# exit()

	if False:
		#benchmarking
		print("####################################")
		print("Benchmarking potential derivatives")
		import time
		n_c = 1
		print(n_c)
		Vder = PES.Vder(q_test, n_c)

		start = time.perf_counter()
		n = 100
		for i in range(n):
			Vder = PES.Vder(q_test, n_c)
		stop = time.perf_counter()
		#print(convder[..., 0])
		print(f"Time total (s): {stop-start:.3f}")
		print(f"Time per call (s): {(stop-start)/n:.4f}")

	exit()

	print("####################################")
	print("Testing the full potential")

	print("Gradient test")
	G_analytic = PES.potential_der(x_test, 1)
	G_numeric = PES.numgrad(x_test).flatten()
	print(G_analytic.shape)
	ratio_G = G_analytic / G_numeric
	print(f"Max ratio (should be close to 1): {np.nanmax(ratio_G)}")
	print(f"Min ratio (should be close to 1): {np.nanmin(ratio_G)}")
	print(ratio_G)


	print("Hessian test")
	H_analytic = PES.potential_der(x_test, 2)
	H_numeric = PES.numhess(x_test).reshape((12,12))
	print(H_analytic.shape)
	ratio_H = H_analytic / H_numeric
	print(f"Max ratio (should be close to 1): {np.nanmax(ratio_H)}")
	print(f"Min ratio (should be close to 1): {np.nanmin(ratio_H)}")
	print(ratio_H[0])

	exit()

	print("Third test")
	T_analytic = PES.third_der(x_test)
	T_numeric = PES.numthird(x_test).reshape((12,12,12))
	print(T_analytic.shape)
	ratio_T = T_analytic / T_numeric
	print(f"Max ratio (should be close to 1): {np.nanmax(ratio_T)}")
	print(f"Min ratio (should be close to 1): {np.nanmin(ratio_T)}")
	print(ratio_T[0][0])

	exit()

	print("Fourth test")
	F_analytic = PES.fourth_der(x_test)
	F_numeric = PES.numfourth(x_test)
	print(F_analytic.shape)
	ratio_F = F_analytic / F_numeric
	print(f"Max ratio (should be close to 1): {np.nanmax(ratio_F)}")
	print(f"Min ratio (should be close to 1): {np.nanmin(ratio_F)}")
	print(ratio_F)
