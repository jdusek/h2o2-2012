import numpy as np
from h2o2_2012 import h2o2_12

"""
This will take some time to run.
"""

PES_symb = h2o2_12(sympy_mode=True)

x_test = np.array(
	[
		[ 1.039938,    -0.919890,     0.283607],
		[ 0.926931,    -0.000000,    -0.308872],
		[-0.372107,     0.750000,    -0.308874],
		[-0.101543,     1.545759,     0.012939]
	]
)



#print("Symbolic gradient")
#G_symb = PES_symb.gradient(x_test)

#print("Symbolic hessian")
#G_symb = PES_symb.hessian(x_test)

#print("Symbolic third ders")
#G_symb = PES_symb.third_der(x_test)

print("Symbolic fourth ders")
G_symb = PES_symb.fourth_der(x_test)