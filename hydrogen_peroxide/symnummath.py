"""
# Symnummath

This is a companion module to symderiv.
It provides functions that work with numpy and optionally with sympy for enhanced functionality.
If you only use the basic numpy functionality, you do **not** need to have sympy installed to import this module.


The two main functions of this module are:
1. It provides mathematical functions that can be used both with numpy and sympy.
2. It provides the function memoized_der, which either computes a derivative symbolically, or uses a previous symbolic calculation.

Mathematical functions that work out of the box (for numpy arrays of sympy symbols):
- Addition and subtraction (+ and -)
- Exponentiation (e. g. x**2, or x**(1./2) )
- The dot product (np.dot) and the cross product (np.cross)
"""
from typing import Callable
from functools import cache
import numpy as np
from opt_einsum import contract
import dill

def sin(x, sympy_mode=False):
	if sympy_mode:
		from sympy import sin
		res = sin(x)
	else:
		res = np.sin(x)
	return res


def cos(x, sympy_mode=False):
	if sympy_mode:
		from sympy import cos
		res = cos(x)
	else:
		res = np.cos(x)
	return res


def tan(x, sympy_mode=False):
	if sympy_mode:
		from sympy import tan
		res = tan(x)
	else:
		res = np.tan(x)
	return res


def arcsin(x, sympy_mode=False):
	if sympy_mode:
		from sympy import asin
		res = asin(x)
	else:
		res = np.arcsin(x)
	return res


def arccos(x, sympy_mode=False):
	if sympy_mode:
		from sympy import acos
		res = acos(x)
	else:
		res = np.arccos(x)
	return res

def arctan2(x, y, sympy_mode=False):
	if sympy_mode:
		from sympy import atan2
		res = atan2(x, y)
	else:
		res = np.arctan2(x, y)
	return res

def sqrt(x, sympy_mode=False):
	if sympy_mode:
		from sympy import asin
		res = asin(x)
	else:
		res = np.arcsin(x)
	return res


def log(x, sympy_mode=False):
	if sympy_mode:
		from sympy import log
		res = log(x)
	else:
		res = np.log(x)
	return res


def exp(x, sympy_mode=False):
	if sympy_mode:
		from sympy import exp
		res = exp(x)
	else:
		res = np.exp(x)
	return res

@cache
def read_file(path: str):
	"""Read files, but it's memoised (cached)."""
	file = open(path, "rb")
	res = dill.load(file)
	return res

def memoized_der(x: np.ndarray, func: Callable, dern: int, name: str, sympy_mode: bool, lambdify: bool = False):
		"""
		In sympy_mode computes the derivative symbolically and then saves it.
		In numpy_mode loads the saved derivative function and computes the derivative.
		"""
		oname = name
		if sympy_mode:
			import symderiv as sd
			if dern == 1:
				diff = sd.grad
			elif dern == 2:
				diff = sd.hess
			elif dern == 3:
				diff = sd.third
			elif dern == 4:
				diff = sd.fourth
			resfun = diff(x, func, lambdify=True)

			with open(oname, 'wb') as file:
				dill.dump(resfun, file)
		else:
			resfun = read_file(oname)

		if lambdify:
			return resfun
		else:
			res = resfun(x)
			return res



def chain_rule(f: list, g: list, n: int):
	"""
	Implements Faà di Bruno's formula, i. e.
	computes the nth derivative of the function
	f(g(x))
	See also: https://en.wikipedia.org/wiki/Fa%C3%A0_di_Bruno%27s_formula#Multivariate_version


	f, g: list of derivatives of the functions
	f[0] -> not important (but something has to be there)
	f[1] -> gradient
	f[2] -> hessian
	etc.
	"""

	if n == 1:
		res = contract("i, ai->a", f[1], g[1])
	elif n == 2:
		res = contract("ij, ai, bj->ab", f[2], g[1], g[1])
		res += contract("i, abi->ab", f[1], g[2])
	elif n == 3:
		res =  contract("ijk, ai, bj, ck->abc", f[3], g[1], g[1], g[1])
		res += contract("i, abci->abc", f[1], g[3])
		res += contract("ij, ai, bcj->abc", f[2], g[1], g[2])
		res += contract("ij, bi, acj->abc", f[2], g[1], g[2])
		res += contract("ij, ci, abj->abc", f[2], g[1], g[2])
	elif n == 4:
		res =  contract("ijkl, ai, bj, ck, dl->abcd", f[4], g[1], g[1], g[1], g[1])
		res += contract("i, abcdi->abcd", f[1], g[4])
		#4 terms
		res += contract("ij, ai, bcdj->abcd", f[2], g[1], g[3])
		res += contract("ij, bi, acdj->abcd", f[2], g[1], g[3])
		res += contract("ij, ci, abdj->abcd", f[2], g[1], g[3])
		res += contract("ij, di, abcj->abcd", f[2], g[1], g[3])
		#3 terms
		res += contract("ij, adi, bcj->abcd", f[2], g[2], g[2])
		res += contract("ij, bdi, acj->abcd", f[2], g[2], g[2])
		res += contract("ij, cdi, abj->abcd", f[2], g[2], g[2])
		#6 terms
		res += contract("ijk, ai, dk, bcj->abcd", f[3], g[1], g[1], g[2])
		res += contract("ijk, bi, dk, acj->abcd", f[3], g[1], g[1], g[2])
		res += contract("ijk, ci, dk, abj->abcd", f[3], g[1], g[1], g[2])
		res += contract("ijk, adi, bj, ck->abcd", f[3], g[2], g[1], g[1])
		res += contract("ijk, ai, bdj, ck->abcd", f[3], g[1], g[2], g[1])
		res += contract("ijk, ai, bj, cdk->abcd", f[3], g[1], g[1], g[2])
	return res
